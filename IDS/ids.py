# IDS by Verena Menzel et al. (v.me.menzel@utwente.nl)
# central entity to manage the whole IDS and calling further classes to
# load the data and prepare it
# let the PF run
# check the results against the ground truth
# also does the GUI through console printing

from ids_helper import ids_helper
from ids_pf_solver import ids_pf_solver
from scada_com import scada_com


def main():

    # 1 = Optimal Power Flow, 2 = Panda Power
    use_solver = 1

  #  tolerance = [0, 0.0001, 0.00025, 0.0005, 0.00075, 0.001, 0.00125, 0.0015, 0.00175]
 # 0.008, 0.0085, 0.009, 0.01, 0.015, 0.02
   # tolerance = [0.008, 0.0085, 0.009, 0.01, 0.015, 0.02]

    tolerance = [0.00175] 

    global file_location_ref_node
    file_location_ref_node = '../Simulation/outputs/ref_node_pp.csv'

    global file_location_node_2
    file_location_node_2 = '../Simulation/outputs/node_2_pp.csv'

    global file_location_node_3
    file_location_node_3 = '../Simulation/outputs/node_3_pp.csv'

   

    FILENAME_0 = 'outputs/' + 'trash' + '.csv'
    fd = open(FILENAME_0, 'a+')
    fd.write("Tolerance;Round;Error count per validation; error count per step; ref count; node count; true positives:; true negatives; false positives; false negatives")
    fd.write("\n")
           
    # tolerance
    for k in range(1): 

        # run j times to ensure proper randomness
        for j in range(1): 
            ids_helper.error_count_ref = 0
            ids_helper.error_count_load_nodes = 0
            
            print("Starting the IDS.\n")
            attack_sequence = ids_helper.calculate_attack_sequence()
            
            # over the whole data set
            for i in range(96):
                
                ref_data = ids_helper.read_data_from_simulation(
                    file_location_ref_node, i)
                load_data_2 = ids_helper.read_data_from_simulation(
                    file_location_node_2, i)
                load_data_3 = ids_helper.read_data_from_simulation(
                    file_location_node_3, i)

                data_complete = ids_helper.accumulate_SCADA_data(
                    ref_data, load_data_2, load_data_3)

              #  print("Read data from the SCADA network.\nStarting PF calculation.")

                data_reduced = ids_helper.reduce_data_for_calculation(
                    data_complete, i, attack_sequence)

                model_calculated = ids_pf_solver.calculate_PF(data_reduced, use_solver)
              #  print("Calculated PF based on SCADA network data.\nComparing to recieved results")

                ids_helper.check_results(
                    model_calculated, tolerance[k], i, attack_sequence)

                print("Time step " + str(i+1) + " done.\n\n")

            print("Error count per validation = " + str(ids_helper.error_total_detail))
            print("Error count per step = " + str(ids_helper.error_total_step))
            print("True positives: " + str(ids_helper.true_positives))
            print("True negatives: " + str(ids_helper.true_negatives))
            print("False positives: " + str(ids_helper.false_positives))
            print("False negatives: " + str(ids_helper.false_negatives))
           # print("Shutting down the IDS.")

        
            fd.write(str(tolerance[k])+ ";" + str(j)+ ";" +  str(ids_helper.error_total_detail)+ ";" + str(ids_helper.error_total_step) + ";" + str(ids_helper.error_count_ref) + ";" + str(ids_helper.error_count_load_nodes) + ";" + str(ids_helper.true_positives)+ ";" +  str(ids_helper.true_negatives) + ";" +  str(ids_helper.false_positives) + ";" + str(ids_helper.false_negatives))
            fd.write("\n")

            ids_helper.false_negatives = 0
            ids_helper.false_positives = 0
            ids_helper.true_negatives = 0 
            ids_helper.true_positives = 0
            ids_helper.error_total_detail = 0
            ids_helper.error_total_step = 0 
            ids_helper.error_count_ref = 0
            ids_helper.error_count_load_nodes = 0

    fd.close() 

if __name__ == "__main__":
    main()

