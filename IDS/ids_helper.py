# class featuring various helper function that do the legwork for the IDS
# featuring loading data and comparing the results form the PF solving with the ground truth
import random
import csv
from scada_com import scada_com


class ids_helper():

    error_total_step = 0
    error_total_detail = 0
    false_positives = 0
    false_negatives = 0
    true_positives = 0
    true_negatives = 0
    attacks = 0

    error_count_ref = 0
    error_count_load_nodes = 0 

    global current_attack_value


    # loading data from files
    def read_data_from_simulation(file_location, i):
        with open(file_location) as csvfile:
            csvReader = csv.reader(csvfile, delimiter=';')

            # skipping header and further information
            csvReader.__next__()
            csvReader.__next__()

            # skip the previous time steps
            for j in range(i):
                csvReader.__next__()

            # reading the actual data for one time step
            data_row = csvReader.__next__()

        # print(data_row)
        return data_row

    # creating data structure from loaded file data + cretaing ground truth
    def accumulate_SCADA_data(ref_node_data, load_data_2, load_data_3):

        data = []
        node_list = [1, 2, 3]
        connection_list = {(1, 2), (1, 3)}

        global v_values
        v_values = {1:  float(ref_node_data[3]), 2:  float(
            load_data_2[3]), 3:  float(load_data_3[3])}

        global theta_values
        theta_values = {1:  1, 2:  float(
            load_data_2[4]), 3:  float(load_data_3[4])}

        global p_values
        p_values = {1:  float(ref_node_data[1]), 2:  float(
            load_data_2[1]), 3:  float(load_data_3[1])}

        global q_values
        q_values = {1:  float(ref_node_data[2]), 2:  float(
            load_data_2[2]), 3:  float(load_data_3[2])}

        R = {(1, 2): float(0.01), (1, 3): float(0.01)}
        X = {(1, 2): float(0.1), (1, 3): float(0.1)}

        data = [node_list, connection_list, theta_values.copy(), p_values.copy(),
                q_values.copy(), R, X]

        return data

    # helper functions to easier calculate the difference
    def calc_difference_in_percent(a, b):
        return abs(1-abs((a/b)))

    # checking results either exact or 0.1% error margin
    def check_results(model_calculated, tolerance, step, attack_sequence):

        FILENAME_0 = 'outputs/' + 'scenario_for_fig4_newnew' + '.csv'
        fd = open(FILENAME_0, 'a+')
        #fd.write("p_ref;q_ref;t_ref;v_ref;p_ref_att;q_ref_att;t_ref_att;v_ref_att")
        #fd.write(str((p_values[1]))+";"+str((q_values[1]))+";"+str(-model_calculated[1]["P"])+";"+str(-model_calculated[1]["Q"])+ ";\n")
        #fd.write(str(theta_values[2])+";"+str(theta_values[3])+";"+str(model_calculated[2]["Theta"])+";"+str(model_calculated[3]["Theta"])+";"+str(v_values[2])+";"+str(v_values[3])+";"+str(model_calculated[2]["V"])+";"+str(model_calculated[3]["V"]))
        fd.write(str(theta_values[3]) + ";" + str(model_calculated[3]["Theta"])+"\n")
        #fd.write("\n")
        #fd.write(str(v_values[1]) + "    " + str(model_calculated[1]["V"]))
        #fd.write("\n")

        error_count = 0

        if (ids_helper.calc_difference_in_percent(model_calculated[2]["V"], v_values[2]) > tolerance):
            print("Alert! Voltage at node 2 does not match")
            error_count += 1
            ids_helper.error_count_load_nodes += 1

        if (ids_helper.calc_difference_in_percent(model_calculated[3]["V"], v_values[3]) > tolerance):
            print("Alert! Voltage at node 3 does not match")
            error_count += 1            
            ids_helper.error_count_load_nodes += 1

        if (ids_helper.calc_difference_in_percent(model_calculated[2]["Theta"], theta_values[2]) > tolerance):
            print("Alert! Angle at node 2 does not match")
            error_count += 1
            ids_helper.error_count_load_nodes += 1

        if (ids_helper.calc_difference_in_percent(model_calculated[3]["Theta"], theta_values[3]) > tolerance):
            print("Alert! Angle at node 3 does not match")
            error_count += 1
            ids_helper.error_count_load_nodes += 1

        if (ids_helper.calc_difference_in_percent(model_calculated[1]["P"], p_values[1]) > tolerance):
            print("Alert! P at ref node does not match")
            error_count += 1
            ids_helper.error_count_ref += 1

        if (ids_helper.calc_difference_in_percent(model_calculated[1]["Q"],  q_values[1]) > tolerance):
            print("Alert! Q at ref node does not match")
            error_count += 1
            ids_helper.error_count_ref += 1

        if (error_count):
            ids_helper.error_total_step = ids_helper.error_total_step+1
            ids_helper.error_total_detail = ids_helper.error_total_detail + error_count
            print(str(error_count) + " violation(s) found in this time step.")

            if (step in attack_sequence):
                ids_helper.true_positives = ids_helper.true_positives+1
               

            else:
                ids_helper.false_positives = ids_helper.false_positives+1
        else:
            print("No violations found in this time step.")
            if (step in attack_sequence):
                ids_helper.false_negatives = ids_helper.false_negatives+1
            else:
                ids_helper.true_negatives = ids_helper.true_negatives+1

        print("___________________")

    # sanitzing from ground truth to input measurements

    def reduce_data_for_calculation(data_complete, step, attack_sequence):

        data_reduced = data_complete.copy()

        #FILENAME_0 = 'outputs/' + 'scenario_2b_p_q_2_3' + '.csv'
       #fd = open(FILENAME_0, 'a+')
      #  fd.write("p_2;q_2;p_3;q_3;p_2_att;q_2_att;p_3_att;q_3_att")
      #  fd.write(str(data_complete[3][2])+";" +str(data_complete[3][3])+";"+str(data_complete[4][2])+";"+str(data_complete[4][3])+";")

        # clear out data for the power flow calculation
        data_reduced[2][2] = 0
        data_reduced[2][3] = 0
        data_reduced[3][1] = 0
        data_reduced[4][1] = 0

        # attack happens here while communicating the data
        data_reduced = scada_com.scenario_new(
            data_reduced, step, attack_sequence)

      #  fd.write(str(data_reduced[3][2])+";"+str(data_reduced[3][3]) +";"+str(data_reduced[4][2])+";"+str(data_reduced[4][3]))

      #  fd.write("\n")
      #  fd.close()
        return data_reduced

    def calculate_attack_sequence():
        sequence = []
        sequence = random.sample(range(96), 24)
        sequence.sort()
       # sequence = [2, 4, 6, 7, 15, 22, 25, 29, 32, 35, 40, 42, 50, 53, 58, 60, 61, 66, 68, 79, 86, 88, 89, 90]
        print(sequence)
        return sequence
