# ids_pf_solver.py
# hosts the heart of the IDS, the actual PF solving on prepared and sanitized data
# can be extende with further PF solvers

import contextlib
import io
from pyomo.environ import *
import create_pf_model as pf
import pandapower as pp
from contextlib import contextmanager
import time 
import csv
import numpy

class ids_pf_solver():

    def print_time_to_file(time): 
            FILENAME_0 = 'outputs/' + 'timeseries' + '.csv'
            fd = open(FILENAME_0, 'a+')
            fd.write(str(time))
            fd.write("\n")
            fd.close()

    # switch between power flow solvers
    def calculate_PF(data, use_solver):
        if(use_solver == 1):
            model = ids_pf_solver.use_solver_one(data)

        if(use_solver == 2):
            model = ids_pf_solver.use_solver_two(data)

        return model

    # Ipopt
    def use_solver_one(data):
        # Parameters
        Vnom = 1   # kV
        Snom = 1000  # kVA
        Vmin = 0.80
        Vmax = 1.05
        Pi = 3.14159265359

        # to prevent excessiv terminal output
        with contextlib.redirect_stdout(io.StringIO()):
            model = pf.create_1_ph_pf_model(Vnom, Snom, Vmin, Vmax, Pi, data)
            # Define the Solver
            solver = SolverFactory('ipopt')
            # Solve the model
            start_time = time.time()
            solver.solve(model, tee=True) 
            elapsed_time = time.time() - start_time

        # sanitizing the results for checking later
        san_model = {}

        for i in (n+1 for n in range(3)):
            san_model[i] = {"V": sqrt(model.V[i].value), "Theta":  model.theta[i].value,
                            "P":  model.PS[i].value, "Q":  model.QS[i].value}
        # print(san_model)
        return san_model

    # Panda Power Standard Solver
    def use_solver_two(data):
        # create empty net
        net = pp.create_empty_network()

        typ1 = {"r_ohm_per_km": 0.01, "x_ohm_per_km": 0.1,
                "c_nf_per_km": 200, "max_i_ka": 100}
        pp.create_std_type(net, name="verenas_type", data=typ1, element="line")

        # create buses
        b1 = pp.create_bus(net, vn_kv=1.5, name="Ref bus")
        b2 = pp.create_bus(net, vn_kv=1.5, name="Node 2")
        b3 = pp.create_bus(net, vn_kv=1.5, name="Node 3")

        # create bus elements
        pp.create_ext_grid(
            net, bus=b1, vm_pu=data[2][1], name="Grid Connection")
        pp.create_load(net, bus=b2, p_mw=data[3]
                       [2], q_mvar=data[4][2], name="Load")
        pp.create_load(net, bus=b3, p_mw=data[3]
                       [3], q_mvar=data[4][3], name="Load")

        # create branch elements
        pp.create_line(net, from_bus=b1, to_bus=b2, length_km=2.25,
                       name="Line", std_type="verenas_type")
        pp.create_line(net, from_bus=b1, to_bus=b3, length_km=2.25,
                       name="Line", std_type="verenas_type")

        # Solve the model
        start_time = time.time()
        pp.runpp(net)
        elapsed_time = time.time() - start_time
        print("Elapsed time: {}".format(elapsed_time))

        # sanitizing the results for checking later
        san_model = {}

        for i in (n+1 for n in range(3)):
            san_model[i] = {"V": net.res_bus["vm_pu"][i-1], "Theta": net.res_bus["va_degree"]
                            [i-1], "P":  net.res_bus["p_mw"][i-1], "Q": net.res_bus["q_mvar"][i-1]}

        return san_model

   