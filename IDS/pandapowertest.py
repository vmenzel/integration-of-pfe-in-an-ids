import pandapower as pp
import pandapower.networks
import pandapower.topology
import pandapower.plotting
import pandapower.converter
import pandapower.estimation

import pandapower.test


def main():
    # create empty net
    net = pp.create_empty_network()

    typ1 = {"r_ohm_per_km": 0.01, "x_ohm_per_km": 0.1,
            "c_nf_per_km": 200, "max_i_ka": 100}
    pp.create_std_type(net, name="verenas_type", data=typ1, element="line")
    print(pp.available_std_types(net))

    # create buses
    b1 = pp.create_bus(net, vn_kv=1.5, name="Ref bus")
    b2 = pp.create_bus(net, vn_kv=1.5, name="Node 2")
    b3 = pp.create_bus(net, vn_kv=1.5, name="Node 3")

    # create bus elements
    pp.create_ext_grid(net, bus=b1, vm_pu=1.00, name="Grid Connection")
    pp.create_load(net, bus=b2, p_mw=0.9, q_mvar=0.6, name="Load")
    pp.create_load(net, bus=b3, p_mw=2.0, q_mvar=1.8, name="Load")

    # create branch elements
    pp.create_line(net, from_bus=b1, to_bus=b2, length_km=2.25,
                   name="Line", std_type="verenas_type")
    pp.create_line(net, from_bus=b1, to_bus=b3, length_km=2.25,
                   name="Line", std_type="verenas_type")

    print(net)
    pp.runpp(net)
    print(net.res_bus)


if __name__ == "__main__":
    main()
