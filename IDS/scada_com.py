# class simulating the attacks happening over the SCADA communication
# can be adpated and extended to feature different sorts of attacks
# could be improved to work with real traffic instead of an already sanitized array
import random

from matplotlib.rcsetup import validate_color_or_auto
import ids_helper


class scada_com():

    ids_helper.current_attack_value = 1.0001

    # no attack, no manipulation
    def scenario_0(data, step, attack_sequence):
        # if(step%4==0)

        return data

    # no attack, but measurement inaccuracies
    def scenario_1(data, step, attack_sequence):

        if (step in attack_sequence):  # hier muss ich noch ueberlegen, wie sinnvoll das so ist
            a = random.randint(0, 1)
            if a == 0:
                value = 0.999
            else:
                value = 1.001

            b = random.randint(1, 4)
            if b == 4:
                data[3][2] = value * data[3][2]
            elif b == 1:
                data[3][3] = value * data[3][3]
            elif b == 2:
                data[4][2] = value * data[4][2]
            else:
                data[4][3] = value * data[4][3]

        return data

    # attack against one load node at a time
    def scenario_2(data, step, attack_sequence):

        if (step in attack_sequence):
        
            a = random.randint(0, 1)
            if a == 0:
                data[3][2] = 0.99 * data[3][2]
                data[3][3] = 0.99 * data[3][3]

            else:
                data[4][2] = 0.99 * data[4][2]
                data[4][3] = 0.99 * data[4][3]
        return data

    # attack against two load nodes at the same time
    def scenario_3(data, step, attack_sequence):
        if (step in attack_sequence):
            a = random.randint(0, 1)
            if a == 0:
                data[3][2] = 0.99 * data[3][2]
                #data[3][3] = 0.99 * data[3][3]

                data[4][2] = 1.01 * data[4][2]
                #data[4][3] = 1.01 * data[4][3]
            else:
                data[3][2] = 1.01 * data[3][2]
                #data[3][3] = 1.01 * data[3][3]

                data[4][2] = 0.99 * data[4][2]
               # data[4][3] = 0.99 * data[4][3]

        return data

    # attack against two load nodes at the same time, with interleaving
    def scenario_4(data, step, attack_sequence):
        if (step in attack_sequence):
            a = random.randint(0, 1)
            if a == 0:
                data[3][2] = 0.99 * data[3][2]
                #data[3][3] = 0.99 * data[3][3]

                data[4][2] = 0.99 * data[4][2]
               # data[4][3] = 0.99 * data[4][3]
            else:
                data[3][2] = 1.01 * data[3][2]
              #  data[3][3] = 1.01 * data[3][3]

                data[4][2] = 1.01 * data[4][2]
               # data[4][3] = 1.01 * data[4][3]

        return data

   # attack against two load nodes at the same time, with interleaving
    def scenario_new(data, step, attack_sequence):
        if (step in attack_sequence):
               # print( ids_helper.current_attack_value)
                data[3][3] = ids_helper.current_attack_value * data[3][3]
               # data[3][3] = ids_helper.current_attack_value * data[3][3]

                ids_helper.current_attack_value = float(ids_helper.current_attack_value) + float(0.0002)
                
               # print( ids_helper.current_attack_value)

        return data
