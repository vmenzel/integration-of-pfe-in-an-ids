# visualisation.py

# Load Matplotlib and data wrangling libraries.
import matplotlib.pyplot as plt
import csv

with open('./outputs/scenario_for_fig4_newnew.csv', 'r') as scenariofile:

    scenario = csv.reader(scenariofile, delimiter=';')

    # with open('./../Simulation/outputs/node_3_pp.csv','r') as scenariofile2:

    #scenario2 = csv.reader(scenariofile2, delimiter=';')

    timesteps = []
    p_2 = []
    q_3 = []
    q_2 = []
    q_3 = []
    p_2_a = []
    p_3_a = []
    q_2_a = []
    q_3_a = []
    p_ref = []
    p_ref_a = []
    q_ref = []
    q_ref_a = []

    t_2 = []
    t_2_a = []
    v_3 = []
    v_3_a = []

    # scenario 3
    attack = [0, 4, 12, 14, 15, 20, 22, 26, 28, 37,
              39, 47, 49, 56, 58, 60, 63, 67, 73, 83, 84, 87]
    #attack = [0, 2, 7, 8, 11, 19 ]

    # scenario 4

    # last version
    # [2, 4, 6, 7, 15, 22, 25, 29, 32, 35, 40, 42, 50, 53, 58, 60, 61, 66, 68, 79, 86, 88, 89, 90]
    attack_true = [32, 35, 40, 42, 50, 53, 58,
                   60, 61, 66, 68, 79, 86, 88, 89, 90]
    attack_false = [2, 4, 6, 7, 15, 22, 25, 29]

    # for i in range(95):
    #row = scenario.__next__()

    for i in range(96):
        timesteps.append(i)

        row = scenario.__next__()
        #row2 = scenario2.__next__()

        # p_2.append(float(row[1]))
        # q_2.append(float(row[]))
        # p_3.append(float(row2[2]))
        # q_3.append(float(row[7]))
        # p_2_a.append(float(row[4]))
        # q_2_a.append(float(row[5]))
        # p_3_a.append(float(row[6]))
        # q_3_a.append(float(row[7]))
        p_ref_a.append(float(row[0]))
        p_ref.append(float(row[1]))
        # q_ref_a.append(float(row[3]))
        # q_ref.append(float(row[1]))

        # t_2.append(float(row[0]))
        # v_3.append(float(row[5]))
        # t_2_a.append(float(row[2]))
        # v_3_a.append(float(row[7]))

# scenario 0 p values for node 2 & 3
plt.plot(timesteps, p_ref_a, color='b', linestyle='dashed',
         marker='', label="angle calculated", lw=5)
plt.plot(timesteps, p_ref, color='g', linestyle='dashed',
         marker='', label="angle measured", lw=5)

# scenario 1 p values for node 2
#plt.plot(timesteps, p_2, color='b', linestyle = 'dashed', marker = 'o',  label="original")
#plt.plot(timesteps, p_2_a, color='r', linestyle = 'dotted', marker = 'o',  label="with manipulation")

# scenario 1 q values for node 3
#plt.plot(timesteps, q_3, color='b', linestyle = 'dashed', marker = 'o',  label="original")
#plt.plot(timesteps, q_3_a, color='r', linestyle = 'dotted', marker = 'o',  label="with manipulation")

# scenario 2 p values for ref node
#plt.plot(timesteps, q_2, color='b', linestyle = 'dashed', marker = '', label="measured voltage angle at load node 2")
#plt.plot(timesteps, q_3, color='k', linestyle = 'dashed', marker = '', label="calculated voltage angle by the IDS")

mark = [timesteps.index(i) for i in attack_true]

mark_f = [timesteps.index(i) for i in attack_false]
# print(attack)
# print(len(attack))

plt.plot(attack_true, [p_ref_a[i] for i in mark], color="r",
         ls="", marker="D", label="Attack, detected", markersize=15)

plt.plot(attack_false, [p_ref_a[i] for i in mark_f], color="k",
         ls="", marker="D", label="Attack, undetected", markersize=15)

#plt.plot(attack_false,[q_3[i] for i in mark_f], color = "g", ls="", marker="o", label="Attack, no alert")

# scenario 3b q values for ref node
#plt.plot(timesteps, q_ref, color='b', linestyle = 'dashed', marker = 'o', label="actual Q")
#plt.plot(timesteps, q_ref_a, color='r', linestyle = 'dotted', marker = 'x', label="calculated Q")


# scenario 3a t values for node 2
#plt.plot(timesteps, v_3, color='b', linestyle = 'dashed', marker = 'o', label="actual Voltage")
#plt.plot(timesteps, v_3_a, color='r', linestyle = 'dotted', marker = 'x', label="calculated Voltage")

# plt.gca().invert_yaxis()

#plt.title("A", fontsize =30)
plt.xlabel("time step", fontsize=40)
plt.ylabel("voltage angle Θ [°]", fontsize=40)
plt.xticks(fontsize=25)
plt.yticks(fontsize=25)

plt.rc('legend', fontsize=30)  # using a size in points
#plt.ylim(bottom=-2, top=10)

plt.legend(loc='lower left')
# plt.yticks([])
plt.legend()

# Show the plot
plt.show()
