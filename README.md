# Securing Smart Grids Locally using a Power Flow-based Intrusion Detection System

As the ongoing energy transition requires more communication infrastructure in the electricity grid, this introduces new possible attack vectors. 
Current intrusion detection approaches for cyber attacks often neglect the underlying physical environment, which makes it especially hard to detect data injection attacks.
We follow a process-aware approach to evaluate the communicated measurement data within the electricity system in a context-sensitive way and to detect manipulations in the communication layer of the SCADA architecture. 
This paper proposes a sophisticated tool for intrusion detection, which integrates power flow analysis in real-time and can be applied locally at field stations mainly at the intersection between the medium and low voltage grid.  
Applicability is illustrated using a simulation testbed with a typical three-node architecture and six different (attack) scenarios. 
Results show that the sensitivity parameter of the proposed tool can be tuned in advance such that attacks can be detected reliably.

## Directory overview

- **IDS**: implementation of IDS utilizing PFE solving in a three node situation. The IDS can be started with ``python ids.py``, in the same file the used algorithm as well as input files can be set. The attacks are defined in ``scada_com.py``. 
    - **output**: output files of the IDS, which contain data showing the actual and the calculated values, depending on the scenarios 
    - **visualisation.py**: python script used for creating the figures
    - **img**: figures used in the paper based on output files 
- **Simulation**: testbed to create the simulated grid data for the IDS, two versions of the testbed are available, depending if OPF or PandaPower should be used. Simulation can be run via ``python sim_OPF.py`` or ``python sim_PP.py``
    - **output**: when running the simulation, output files are stored here 
    - **output_base**: collection of the standard output files from the simulation, that were used for testing the IDS 

## Authors and Citation 
The theoretical foundations and background of this repository are described in: 

V. Menzel, N. B. Arias, J. L. Hurink and A. Remke, "Securing Smart Grids Locally using a Power Flow-based Intrusion Detection System," 2023 IEEE Belgrade PowerTech, Belgrade, Serbia, 2023, pp. 1-9, doi: 10.1109/PowerTech55446.2023.10202762. keywords: {Low voltage;Sensitivity;Current measurement;Intrusion detection;Real-time systems;Smart grids;Reliability;SCADA systems;process-aware;intrusion detection;smart grids;power distribution;power flow analysis},



## Funding

This research is conducted as part of the ISoLATE project (CS.016) funded by NWO.

## External tools & licenses 

This repository uses [Ipopt](https://github.com/coin-or/Ipopt), which is available under [Eclipse Public License - v 2.0](https://github.com/coin-or/Ipopt/blob/stable/3.14/LICENSE), as well as [PandaPower](https://github.com/e2nIEE/pandapower), which is available under [ 3-clause BSD license](https://github.com/e2nIEE/pandapower/blob/v2.11.1/doc/about/license.rst).

The OPF code uses code originally developed by Pedro P. Vergara, TU Eindhoven, p.p.vergara.barrios@tue.nl. 