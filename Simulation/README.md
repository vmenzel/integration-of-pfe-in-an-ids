## Installation instruction 

It is important to have a new version of Python installed like 3.10 and an new version of pip 22.2.1 to install all required requirements using 

```
pip install -r requirements.txt
```