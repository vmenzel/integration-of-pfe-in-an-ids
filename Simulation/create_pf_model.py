from pyomo.environ import *

def create_pf_model(Vnom, Snom, Vmin, Vmax, Pi, Data_Network):
    # Data Processing
    NODES = Data_Network[0]
    LINES = Data_Network[1]
    Tb = Data_Network[2]
    PD = Data_Network[3]
    QD = Data_Network[4]
    R = Data_Network[5]
    X = Data_Network[6]
    
    # Type of Model
    model = ConcreteModel()
    
    # Define Sets
    model.NODES = Set(initialize=NODES)
    model.LINES = Set(initialize=LINES)
    
    # Define Parameters
    model.Vnom = Param(initialize=Vnom, mutable=True)
    model.Snom = Param(initialize=Snom, mutable=True)
    model.Vmin = Param(initialize=Vmin, mutable=True)
    model.Vmax = Param(initialize=Vmax, mutable=True)
    model.Tb = Param(model.NODES, initialize=Tb, mutable=False)
    model.PD = Param(model.NODES, initialize=PD, mutable=True) # Node demand
    model.QD = Param(model.NODES, initialize=QD, mutable=True) # Node demand
    model.R = Param(model.LINES, initialize=R, mutable=True) # Line resistance
    model.X = Param(model.LINES, initialize=X, mutable=True) # Line resistance
    model.Pi = Param(initialize=Pi, mutable=True)

    def R_init_rule(model, i,j):
        return (model.R[i,j])
    model.RM = Param(model.LINES, initialize= R_init_rule) # Line resistance
    
    def X_init_rule(model, i,j):
        return (model.X[i,j])
    model.XM = Param(model.LINES, initialize= X_init_rule) # Line resistance
    
    # Define Variables
    model.P = Var(model.LINES, initialize=0) # Acive power 
    model.Q = Var(model.LINES, initialize=0) # Reacive power 
    model.I  = Var(model.LINES, initialize=0) # Current  lines
    model.Ploss = Var(model.LINES, initialize=0) # Active power losses 
    model.theta_lines = Var(model.LINES, initialize=0) # Active power losses 
    model.theta = Var(model.NODES, initialize=0) # Active power losses 


    def PS_init_rule(model, i):
        if model.Tb[i] == 0:
            temp = 0.0
            model.PS[i].fixed = True
            model.theta[i].fixed = True
        else:
            temp = 0.0
        return temp
    model.PS = Var(model.NODES, initialize = PS_init_rule)  # Active power of the SS
    model.theta = Var(model.NODES, initialize = PS_init_rule) # Angle of the SS

    def QS_init_rule(model, i):
        if model.Tb[i] == 0:
            temp = 0.0
            model.QS[i].fixed = True
        else:
            temp = 0.0
        return temp
    model.QS = Var(model.NODES, initialize = QS_init_rule)  # Active power of the SS
    
    def Angle_init_rule(model, i):
        if model.Tb[i] == 1:
            temp = 0.0
            model.theta[i].fixed = True
        else:
            temp = 0.0
            model.theta[i].fixed = False
        return temp
    model.theta = Var(model.NODES, initialize = Angle_init_rule) # Angle of the SS

    # Voltafe of nodes
    def Voltage_init(model, i):
        if model.Tb[i] == 1:
            temp = 1.0#model.Vnom
            model.V[i].fixed = True
        else:
            temp = 1.0#model.Vnom
            model.V[i].fixed = False
        return temp
    model.V = Var(model.NODES, initialize = Voltage_init)

    def act_loss(model):
        return (sum(model.Ploss[i,j] for i,j in model.LINES))
    model.obj = Objective(rule=act_loss)

    def active_power_flow_rule(model, k):
        return (sum(model.P[j,i] for j,i in model.LINES if i == k ) - sum(model.P[i,j] + model.RM[i,j]*(model.I[i,j]) for i,j in model.LINES if k == i) + model.PS[k] == model.PD[k])
    model.active_power_flow = Constraint(model.NODES, rule=active_power_flow_rule)

    def reactive_power_flow_rule(model, k):
        return (sum(model.Q[j,i] for j,i in model.LINES if i == k ) - sum(model.Q[i,j] + model.XM[i,j]*(model.I[i,j]) for i,j in model.LINES if k == i) + model.QS[k] == model.QD[k])
    model.reactive_power_flow = Constraint(model.NODES, rule=reactive_power_flow_rule)

    def voltage_drop_rule(model, i, j):
        return (model.V[i] - 2*(model.RM[i,j]*model.P[i,j] + model.XM[i,j]*model.Q[i,j]) - (model.RM[i,j]**2 + model.XM[i,j]**2)*model.I[i,j] - model.V[j] == 0)
    model.voltage_drop = Constraint(model.LINES, rule=voltage_drop_rule)

    def define_current_rule (model, i, j):
        return ((model.I[i,j])*(model.V[j]) == model.P[i,j]**2 + model.Q[i,j]**2)
    model.define_current = Constraint(model.LINES, rule=define_current_rule)

    def current_limit_rule(model, i,j):
        return (0, model.I[i,j], None)
    model.current_limit = Constraint(model.LINES, rule=current_limit_rule)
    
    def voltage_limit_rule(model, i):
        return (0,model.V[i],None)
    model.voltage_limit = Constraint(model.NODES, rule=voltage_limit_rule)
    
    def active_power_loss_rule(model, i,j):
        return (model.Ploss[i,j] == model.RM[i,j]*(model.I[i,j]))
    model.power_losses = Constraint(model.LINES, rule = active_power_loss_rule) # Power losses   

    def angle_lines_rule(model, i,j):
        return (model.theta_lines[i,j] == (180/model.Pi) * atan(( model.XM[i,j] * model.P[i,j] - model.RM[i,j] * model.Q[i,j])/(model.V[j] + model.RM[i,j]*model.P[i,j] + model.XM[i,j]*model.Q[i,j]) ) )
    model.angles_lines = Constraint(model.LINES, rule = angle_lines_rule) # Angle 
    def angle_lines_rule_2(model, i,j):
        return (model.theta_lines[i,j] == (model.theta[i] - model.theta[j]) )
    model.angles = Constraint(model.LINES, rule = angle_lines_rule_2) # Angle
    return model

