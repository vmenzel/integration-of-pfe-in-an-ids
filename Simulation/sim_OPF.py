from pyomo.environ import *
import create_pf_model as pf
import math
import pandas as pd
import numpy
import os
import csv


def load_data_from_file(input_file):
    data = []
    with open(input_file) as csvfile:
        csvReader = csv.reader(csvfile, delimiter=";")

        next(csvReader, None)
        for dataRow in csvReader:
            data.append(dataRow)

    return data


def load_data(i, data):
    data = [{'NODES': 1, 'Tb': 1, 'PD': 0, 'QD': 0}, {'NODES': 2, 'Tb': 0, 'PD': float(data[i][1]), 'QD': float(
        data[i][2])}, {'NODES': 3, 'Tb': 0, 'PD': float(data[i][3]), 'QD': float(data[i][4])}]
    df = pd.DataFrame(data)
    print(df)
    return df


def prepare_PQ_values_for_IDS(System_Data_Nodes, Snom, Vnom):
    PQ_values = []

    NODES = [System_Data_Nodes.loc[i, 'NODES']
             for i in System_Data_Nodes.index]

    PQ_values.append({NODES[i]: System_Data_Nodes.loc[i, 'PD']
                     for i in System_Data_Nodes.index})

    PQ_values.append({NODES[i]: System_Data_Nodes.loc[i, 'QD']
                     for i in System_Data_Nodes.index})

    return PQ_values


def processing_system_data_for_pyomo(System_Data_Nodes, System_Data_Lines, Vnom, Pi, Snom):
    # Network Data
    NODES = [System_Data_Nodes.loc[i, 'NODES']
             for i in System_Data_Nodes.index]

    Tb = {NODES[i]: System_Data_Nodes.loc[i, 'Tb']
          for i in System_Data_Nodes.index}

    PD = {NODES[i]: System_Data_Nodes.loc[i, 'PD']
          for i in System_Data_Nodes.index}

    QD = {NODES[i]: System_Data_Nodes.loc[i, 'QD']
          for i in System_Data_Nodes.index}

    LINES = {(System_Data_Lines.loc[i, 'FROM'], System_Data_Lines.loc[i, 'TO'])
             for i in System_Data_Lines.index}

    R = {(System_Data_Lines.loc[i, 'FROM'], System_Data_Lines.loc[i, 'TO']): System_Data_Lines.loc[i, 'R'] / (
        Vnom ** 2 * 1000 / (Snom)) for i in System_Data_Lines.index}

    X = {(System_Data_Lines.loc[i, 'FROM'], System_Data_Lines.loc[i, 'TO']): System_Data_Lines.loc[i, 'X'] / (
        Vnom ** 2 * 1000 / (Snom)) for i in System_Data_Lines.index}

    Data_Network = [NODES, LINES, Tb, PD, QD, R, X]

    print(Data_Network)

    return Data_Network


def write_intro_to_file(node_name):
    FILENAME = 'outputs/' + node_name + '.csv'
    try:
        os.remove(FILENAME)
    except OSError:
        pass
    fd = open(FILENAME, 'w+')
    introrow = node_name + "\nTime step;P;Q;v;Theta;\n"
    fd.write(introrow)
    fd.close()


def write_results_to_file(model, node_name, node_number, time_step, PQ_values=None):
    FILENAME_0 = 'outputs/' + node_name + '.csv'
    fd = open(FILENAME_0, 'a+')

    # creation of csv row for data storage
    csvrow = ""
    csvrow += str(time_step) + ";"

    if PQ_values is None:
        csvrow += str(model.PS[node_number].value) + ";"
        csvrow += str(model.QS[node_number].value) + ";"
    else:
        csvrow += str(PQ_values[0][node_number]) + ";"
        csvrow += str(PQ_values[1][node_number]) + ";"

    csvrow += str(sqrt(model.V[node_number].value)) + ";"
    csvrow += str(model.theta[node_number].value)

    fd.write(csvrow)
    fd.write("\n")
    fd.close()


def main():

    write_intro_to_file("ref_node")
    write_intro_to_file("node_2")
    write_intro_to_file("node_3")

    data = load_data_from_file("input.csv")

    for i in range(96):
        # Importing System Data
        System_Data_Nodes = load_data(i, data)

        System_Data_Lines = pd.read_excel('Lines_3.xlsx', engine='openpyxl')

        # Parameters
        Vnom = 1   # kV
        Snom = 1000  # kVA
        Vmin = 0.80
        Vmax = 1.05
        Pi = 3.14159265359

        PQ_values = prepare_PQ_values_for_IDS(System_Data_Nodes, Snom, Vnom)

        # Preparing System Data for Pyomo
        Data_Network = processing_system_data_for_pyomo(
            System_Data_Nodes, System_Data_Lines, Vnom, Pi, Snom)

        # Create the Model
        model = pf.create_1_ph_pf_model(
            Vnom, Snom, Vmin, Vmax, Pi, Data_Network)

        # Define the Solver
        solver = SolverFactory('ipopt')

        # Solve the model
        solver.solve(model, tee=True)

        write_results_to_file(model, "ref_node", 1, i)
        write_results_to_file(model, "node_2", 2, i, PQ_values)
        write_results_to_file(model, "node_3", 3, i, PQ_values)


if __name__ == "__main__":
    main()
