# create simulation data using pandapower
# by Verena

import csv
from pyomo.environ import *
import create_1_ph_pf_model as pf
import math
import pandas as pd
import numpy
import os
from pyomo.environ import *
import create_1_ph_pf_model as pf
import pandapower as pp
from contextlib import contextmanager


def load_data_from_file(input_file):
    data = []
    with open(input_file) as csvfile:
        csvReader = csv.reader(csvfile, delimiter=";")

        next(csvReader, None)
        for dataRow in csvReader:
            data.append(dataRow)

    return data


# Panda Power Standard Solver
def solve_pf_to_generate_data(data):
    # create empty net
    net = pp.create_empty_network()

    typ1 = {"r_ohm_per_km": 0.01, "x_ohm_per_km": 0.1,
            "c_nf_per_km": 200, "max_i_ka": 100}
    pp.create_std_type(net, name="verenas_type", data=typ1, element="line")

    # create buses
    b1 = pp.create_bus(net, vn_kv=1.5, name="Ref bus")
    b2 = pp.create_bus(net, vn_kv=1.5, name="Node 2")
    b3 = pp.create_bus(net, vn_kv=1.5, name="Node 3")

    # create bus elements
    pp.create_ext_grid(net, bus=b1, vm_pu=data[0], name="Grid Connection")
    pp.create_load(net, bus=b2, p_mw=data[1], q_mvar=data[2], name="Load")
    pp.create_load(net, bus=b3, p_mw=data[3], q_mvar=data[4], name="Load")

    # create branch elements
    pp.create_line(net, from_bus=b1, to_bus=b2, length_km=2.25,
                   name="Line", std_type="verenas_type")
    pp.create_line(net, from_bus=b1, to_bus=b3, length_km=2.25,
                   name="Line", std_type="verenas_type")

    pp.runpp(net)

    # sanitizing the results for checking later
    san_model = {}

    for i in (n+1 for n in range(3)):
        san_model[i] = {"V": net.res_bus["vm_pu"][i-1], "Theta": net.res_bus["va_degree"]
                        [i-1], "P":  net.res_bus["p_mw"][i-1], "Q": net.res_bus["q_mvar"][i-1]}

    return san_model


def write_intro_to_file(node_name):
    FILENAME = 'outputs/' + node_name + '_pp.csv'
    try:
        os.remove(FILENAME)
    except OSError:
        pass
    fd = open(FILENAME, 'w+')
    introrow = node_name + "\nTime step;P;Q;v;Theta;\n"
    fd.write(introrow)
    fd.close()


def write_results_to_file(model, node_name, node_number, time_step, PQ_values=None):
    FILENAME_0 = 'outputs/' + node_name + '_pp.csv'
    fd = open(FILENAME_0, 'a+')

    # creation of csv row for data storage
    csvrow = ""
    csvrow += str(time_step) + ";"

    csvrow += str(model[node_number]["P"]) + ";"
    csvrow += str(model[node_number]["Q"]) + ";"

    csvrow += str(model[node_number]["V"]) + ";"
    csvrow += str(model[node_number]["Theta"])

    fd.write(csvrow)
    fd.write("\n")
    fd.close()


def main():
    write_intro_to_file("ref_node")
    write_intro_to_file("node_2")
    write_intro_to_file("node_3")

    data = load_data_from_file("input.csv")

    for i in range(96):

        model = solve_pf_to_generate_data(data[i])

        write_results_to_file(model, "ref_node", 1, i)
        write_results_to_file(model, "node_2", 2, i)
        write_results_to_file(model, "node_3", 3, i)


if __name__ == "__main__":
    main()

# %%
